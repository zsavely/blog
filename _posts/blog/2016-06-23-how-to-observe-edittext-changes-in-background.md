---
layout: post
title: "How to Observe EditText Changes in Background"
categories: blog
excerpt: ""
tags: [android, edittext, textwatcher, rxjava, rxandroid]
comments: true
share: true
date: 2016-06-23T19:00:00+03:00
---

In this post I'll show some useful tips for observing View changes in the background using [RxAndroid][1] and [RxBinding][2].

Let's start with an easy example of observing EditText changes.

{% highlight java %}
Subscription subscription = RxTextView.textChanges(editText)
    .subscribeOn(AndroidSchedulers.mainThread())
    .subscribe(new Observer<CharSequence>() {
      @Override public void onCompleted() { }
      @Override public void onError(Throwable e) { }
      @Override public void onNext(CharSequence charSequence) { }
    });
{% endhighlight %} 

This example will subscribe and observe `EditText` changes on the main thread. Say we want now to filter the `CharSequence` from the current `EditText` and proceed to the `onNext()` method only if the `CharSequence` is not empty? How do we achieve that in the Observable chain? We can add a function [`filter()`][5]:

{% highlight java %}
.filter(new Func1<CharSequence, Boolean>() {
  @Override public Boolean call(CharSequence charSequence) {
    return charSequence.length() != 0;
  }
})
{% endhighlight %}

In this case function `filter()` doesn't add any problems in terms of freezing the UI because it's super fast to process. But! Say we want to have a really heavy `CharSequence` processing that probably will freeze the UI for a second. 

{% highlight java %}
.filter(new Func1<CharSequence, Boolean>() {
  @Override public Boolean call(CharSequence charSequence) {
    SystemClock.sleep(1000); // Simulate the heavy stuff.
    return charSequence.length() != 0;
  }
})
{% endhighlight %}

We don't want the UI thread to hang and prevent the user from typing the text, so we want to do the `filter` thing on another thread. In order to achieve that, we can just add a line with `observeOn()` before the `filter` function.

{% highlight java %}
.observeOn(Schedulers.computation())
{% endhighlight %}

In this case, we filter the `CharSequence` on the computation thread, however now our `Observer<CharSequence>` will receive the result on the computation thread, too! So if we want to update some views in the `onNext()` method, we need to add a line after the filtering function.

{% highlight java %}
.observeOn(AndroidSchedulers.mainThread())
{% endhighlight %}

Everything seems to be OK by now but there are some issues which might occur while using such approach:

 1. [`rx.exceptions.MissingBackpressureException`][4] will likely occur when the user will be either typing too fast or deleting the symbols too fast. 
 2. Memory leaking if we don't unsubscribe from the `Subscription`. Moreover, if some `View`s were referenced in the `onNext()` method, then there's a potential `NullPointerException`.

To address the issue #1, we can use the [`debounce()`][3] function. It is very useful for obtaining only those items that are not followed by another item within a specified duration[^debounce].

We should add the `debounce()` function before any filtering.

{% highlight java %}
.debounce(500, TimeUnit.MILLISECONDS)
{% endhighlight %}

To address the issue #2, we must unsubscribe from the `Observable<CharSequence>`. If we do that, we will be on the safe side.

{% highlight java %}
@Override protected void onStop() {
  super.onStop();

  if (!subscription.isUnsubscribed()) {
    subscription.unsubscribe();
  }
}
{% endhighlight %}

The resulting rx chain looks like this:

{% highlight java %}
subscription = RxTextView.textChanges(editText)
    .debounce(500, TimeUnit.MILLISECONDS)
    .observeOn(Schedulers.computation())
    .filter(new Func1<CharSequence, Boolean>() {
      @Override public Boolean call(CharSequence charSequence) {
        SystemClock.sleep(1000); // Simulate the heavy stuff.
        return charSequence.length() != 0;
      }
    })
    .subscribeOn(AndroidSchedulers.mainThread())
    .observeOn(AndroidSchedulers.mainThread())
    .subscribe(new Observer<CharSequence>() {
      @Override public void onCompleted() { }
      @Override public void onError(Throwable e) {
        Log.d("RxTesting", "Error.", e);
      }
      @Override public void onNext(CharSequence charSequence) {
        content.setText(charSequence);
        count.setText(String.valueOf(charSequence.length()));
      }
    });
{% endhighlight %}

You can find a sample application [here][6].



  [1]: https://github.com/JakeWharton/RxBinding
  [2]: https://github.com/ReactiveX/RxAndroid  
  [3]: http://reactivex.io/documentation/operators/debounce.html
  [4]: https://github.com/ReactiveX/RxJava/wiki/Backpressure
  [5]: http://reactivex.io/documentation/operators/filter.html
  [6]: https://github.com/zsavely/RxViewSamples/blob/master/app/src/main/java/com/szagurskii/rxviewsample/Part1Activity.java
  
  [^debounce]: The [debounce](https://github.com/ReactiveX/RxJava/wiki/Backpressure#debounce-or-throttlewithtimeout) operator emits only those items from the source Observable that are not followed by another item within a specified duration.
