---
layout: post
title: "How to Deploy APK to GitHub releases with Travis CI"
excerpt: ""
modified:
categories: blog
tags: [travis, android, ci, deploy, releases, github, tags]
comments: true
share: true
image:
  feature:
date: 2016-05-30T09:18:55+03:00
---


[Travis](https://travis-ci.org) is a useful service for building and testing your code. Once you are familiar with Travis, you will likely start building with Travis because it is very easy to start with. Usually, when building Android Project, you want a resulting APK to ship. As Travis builds your APK, you are able to extract that APK through Gradle script and publish it somewhere (even GitHub). But you can do exactly the same with just a few lines in `.travis.yml`. 

3 steps to start publishing APK to GitHub releases through Travis:

### Step #1
Configure your repository on GitHub and make sure that `.travis.yml` is present in the root of your repository. Here's sample `.travis.yml` file:

{% highlight yaml %}
sudo: false

language: android
jdk: oraclejdk8

android:
  components:
  - platform-tools
  - tools
  - build-tools-23.0.3
  - android-23

before_install:
- chmod +x gradlew

script:
- "./gradlew clean build connectedAndroidTest -PdisablePreDex --stacktrace"

notifications:
  email: true

before_cache:
- rm -f $HOME/.gradle/caches/modules-2/modules-2.lock

cache:
  directories:
  - "$HOME/.m2"
  - "$HOME/.gradle"
  - "$HOME/.gradle/caches/"
  - "$HOME/.gradle/wrapper/"
{% endhighlight %}

### Step #2
Download the [Travis CI command line client](https://github.com/travis-ci/travis.rb#installation) and run `travis setup releases` in the root directory of your repository. 

3. Type in your username and password.
4. When asked for file to upload insert `yourapp/build/outputs/apk/yourapp*release*.apk`.
5. Deploy only from `username/Repo`? Type `yes`.
6. Encrypt API key? Yes! This is an important part of keeping access to your repo private.

Now you will be able to see that `travis` added several lines of script in your `travis.yml` file.
They are as follows:

{% highlight yaml %}
deploy:
  provider: releases
  api_key:
    secure: "wy43X9WESaODgKXr..."
  file: "yourapp/build/outputs/apk/yourapp*release*.apk"
  on:
    repo: username/Repo
{% endhighlight %}

Everything is almost OK by now but there are several nuances.

1. At this time Travis will treat `yourapp*release*.apk` like `yourapp*release*.apk` and not like `yourapp...something...release...something.apk`. So there is a parameter in Travis called `file_glob` which should be enabled if you want files to be interpreted as globs (* and ** wildcards). 
2. You should supply `skip_cleanup: "true"` because otherwise Travis may clean all APKs.
3. *(optional)* You may want to specify the branch on which Travis is supposed to build APKs. This should be inserted in the `on:` section like:

{% highlight yaml %}
  on:
  branch: master
    repo: username/Repo
{% endhighlight %}

The resulting `deploy` section:

{% highlight yaml %}
deploy:
  provider: releases
  api_key:
    secure: "wy43X9WESaODgKXr..."
  file: "yourapp/build/outputs/apk/yourapp*release*.apk"
  file_glob: "true"
  skip_cleanup: true
  on:
    branch: master
    repo: username/Repo
{% endhighlight %}

### Step #3

In order to successfully push your APK to GitHub, you need to put a tag on the commit that has just been built. To do that, we will add a new section called `after_success:` where we will execute custom script which sets current build tag. The script `set_tags.sh` is as follows:

{% highlight bash %}
BRANCH="master"

# Are we on the right branch?
if [ "$TRAVIS_BRANCH" = "$BRANCH" ]; then
  
  # Is this not a Pull Request?
  if [ "$TRAVIS_PULL_REQUEST" = false ]; then
    
    # Is this not a build which was triggered by setting a new tag?
    if [ -z "$TRAVIS_TAG" ]; then
      echo -e "Starting to tag commit.\n"

      git config --global user.email "travis@travis-ci.org"
      git config --global user.name "Travis"

      # Add tag and push to master.
      git tag -a v${TRAVIS_BUILD_NUMBER} -m "Travis build $TRAVIS_BUILD_NUMBER pushed a tag."
      git push origin --tags
      git fetch origin

      echo -e "Done magic with tags.\n"
  fi
  fi
fi
{% endhighlight %}

Place this script in the root directory of your project. And then add this 2 lines to `.travis.yml`:

{% highlight yaml %}
after_success:
- sh set_tags.sh
{% endhighlight %}

By now you should be able to build and publish your APKs to GitHub releases sections. It will look like this:

[![GitHub Releases Section][1]][1]

Now you can publish your APKs to GitHub releases with no hassle! A sample project can be found [here](https://github.com/zsavely/AutoDeployExample).

Happy publishing!

[1]: https://i.imgur.com/oF1y48K.png