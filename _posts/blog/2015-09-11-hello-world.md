---
layout: post
title: "Hello World"
categories: blog
tags: [hello-world, hooray]
comments: true
share: true
excerpt: ""
date: 2015-09-11T15:44:55+03:00
---


Hi everybody!

Thanks for visiting my website and reading this. Honestly, I am extremely excited to start a blog. It is like a whole new life is opening to me right now. As more I grew as a software engineer, the more I thought about this day when I finally start a blog.

I hope that this blog will contain interesting and useful information about mobile development and Android in particular. I plan to share useful code snippets, libraries and other interesting stuff that can be somewhat valuable to community.

That's all for now. Again, thanks for reading!

Cheers!
