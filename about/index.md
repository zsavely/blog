---
layout: about
title: About me
excerpt: "Android Blog by Savelii Zagurskii."
modified: 2016-06-03T13:05:38.564948+03:00

---
Hi, my name is Savelii Zagurskii. I am an Android Engineer who loves building great robust applications, writing beautiful code and playing ice hockey!
